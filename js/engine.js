$(function() {

// BEGIN of script WOW.
new WOW().init();



  
  $("#ticker").endlessScroll({ 
    width: "100%", // Ширина строки
    height: "70px", // Высота строки
    steps: -1, // Шаг анимации в пикселях. Если число отрицательное - движение влево, положительное - вправо
    speed: 5, // Скорость анимации (0 - максимальная)
    mousestop: true // Останавливать ли полосу при наведении мыши (да - true, нет - false)
  });
    

  $("#partners").endlessScroll({ 
    width: "100%", // Ширина строки
    height: "220px", // Высота строки
    steps: -1, // Шаг анимации в пикселях. Если число отрицательное - движение влево, положительное - вправо
    speed: 6, // Скорость анимации (0 - максимальная)
    mousestop: true // Останавливать ли полосу при наведении мыши (да - true, нет - false)
  });




$(".main-menu").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top}, 1500);
  });



// recommendation slider
 var testimonialsSlider =  $('.testimonials-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed:4000,
      arrows: false,
      dots: false,
     
  });
$('.testimonials-slider__prev').click(function(){
  $(testimonialsSlider).slick("slickNext");
 });
 $('.testimonials-slider__next').click(function(){
  $(testimonialsSlider).slick("slickPrev");
 });





// Прокрутка вверх при клике

var amountScrolled = 2000;

$(window).scroll(function() {
  if ( $(window).scrollTop() > amountScrolled ) {
    $('a.back-to-top').fadeIn('200');
  } else {
    $('a.back-to-top').fadeOut('200');
  }
});
$('a.back-to-top').click(function() {
  $('html, body').animate({
    scrollTop: 0
  }, 700);
  return false;
});





// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });










})